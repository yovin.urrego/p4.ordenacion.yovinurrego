/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista;

import com.google.gson.*;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Persona;

/**
 *
 * @author Yovin
 */
public class guardasGSON {

    ListaEnlazada<Persona> lista = new ListaEnlazada<>();

    public void guardar() throws PosicionException, IOException {

        Gson json = new Gson();
        Persona[] arrayP = new Persona[10000];
        for (int i = 0; i < 10000; i++) {
            arrayP[i] = lista.obtenerDato(i);
        }
        String jsonString =json.toJson(arrayP);
         FileWriter file = new FileWriter("datos" + File.separatorChar + "Personas.json");
        file.write(jsonString);
        file.flush();
    }

    public String nombre() {
        String banco1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String bancoLetras = "abcdefghijklmnopqrstuvwxyz";
        String bancoVocales = "aeiou";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < (Math.random() * (6 - 4)) + 4; i++) {
            if (i == 0) {
                char caracterAleatorio = banco1.charAt((int) (Math.random() * ((banco1.length() - 1) - 0)) + 0);
                sb.append(caracterAleatorio);
            } else if (i / 2 == (double) i / 2.00) {
                char caracterAleatorio = bancoLetras.charAt((int) (Math.random() * ((bancoLetras.length() - 1) - 0)) + 0);
                sb.append(caracterAleatorio);
            } else {
                char caracterAleatorio = bancoVocales.charAt((int) (Math.random() * ((bancoVocales.length() - 1) - 0)) + 0);
                sb.append(caracterAleatorio);
            }
        }
        return String.valueOf(sb);
    }

    public void agregar() {
        for (int i = 0; i < 10000; i++) {

            String nombre = nombre(), apellido = nombre();
            Integer cedula = 1950083533 + (int) (Math.random() * (100900000 - 1)) + 1;

            lista.insertarCabecera(new Persona(nombre, apellido, cedula));
        }

    }
    
        public static void main(String[] args) {
        // TODO code application logic here
        guardasGSON o = new guardasGSON();
    o.agregar();
        try {
            o.guardar();
        } catch (PosicionException ex) {
            Logger.getLogger(guardasGSON.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(guardasGSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
