/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vista;

import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Persona;

/**
 *
 * @author Yovin
 */
public class NewMain {

    String nombreFichero = "datos" + File.separatorChar+"lista.dat";
    ListaEnlazada<Persona> lista = new ListaEnlazada<>();

    public void agregar() {
        Persona p = new Persona("juan", "antonio", 1950083533);
        this.lista.insertarCabecera(p);
   
        try {
            // TODO code application logic here

            FileOutputStream ficheroSalida = new FileOutputStream(nombreFichero);
             ObjectOutputStream objetoSalida = new ObjectOutputStream(ficheroSalida);
            objetoSalida = new ObjectOutputStream(ficheroSalida);

            // se escriben dos objetos de la clase Persona
            objetoSalida.writeObject(lista);
        } catch (IOException ex) {
            Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void leer() throws ClassNotFoundException, PosicionException {

        ObjectInputStream objetoEntrada = null;
        try {
            FileInputStream ficheroEntrada = new FileInputStream(nombreFichero);
            objetoEntrada = new ObjectInputStream(ficheroEntrada);
            // se leen dos objetos de la clase Persona
          ListaEnlazada<Persona>  p1 = (ListaEnlazada<Persona>) objetoEntrada.readObject();
            System.out.println(p1.obtenerDato(0).getNombre());
        } catch (IOException ex) {
            Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // try {
            NewMain n = new NewMain();
            n.agregar();
          /*  n.leer();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

}
